@extends('layouts.base-template')

@section('breadcrumb')
    <nav class="breadcrumb">
        <span class="breadcrumb-item active">Perhitungan</span>
    </nav>
@endsection

@section('alerts')
    @if(Session::has('errors'))
    <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        @foreach (Session::get('errors')->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
    @endif

    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        {{Session::get('success')}}
    </div>
    @endif
@endsection

@section('content')
<div class="border rounded p-4 m-2">
    <div class="table-responsive">
        <h1>Bobot {{$terhadap->nama_kriteria}} terhadap kriteria lain</h1>
        <table class="table">
            <thead>
                <th>Nama Kriteria</th>
                <th>Bobot nilai</th>
                <th>Aksi</th>
            </thead>
            <tbody>
                @foreach($terhadap->bobot as $value)
                <form method="POST" action="{{route('bobot.update',[$terhadap->id,$value->terhadap->id])}}">
                <tr>
                    <td>{{$value->terhadap->nama_kriteria}}</td>
                    <td width="30%">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <button type="button" data-id="{{$value->terhadap->id}}" class="btn btn-outline-info btn-minus"><i class="fa fa-minus"></i></button>
                            </div>
                            <input value="{{array_search($value->bobot,$bobot)}}" id="input-{{$value->terhadap->id}}" type="text" name="bobot" class="form-control" readonly>
                            <div class="input-group-append">
                                <button type="button" data-id="{{$value->terhadap->id}}" class="btn btn-outline-info btn-plus"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    </td>
                    <td>
                        @csrf
                        <button type="submit" data-id="{{$value->id}}" class="btn btn-primary btn-edit"><i class="fa fa-pencil"></i> Ubah Nilai</button>
                    </td>
                    </tr>
                </form>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection


@section('js')
    <script>
        var valueArr = ['1/9','1/8','1/7','1/6','1/5','1/4','1/3','1/2','1','2','3','4','5','6','7','8','9'];
        $('button.btn-minus').click(function(){
            id = $(this).data('id');
            current = $('#input-'+id).val();
            current_key = valueArr.indexOf(current);
            current_key--;
            if(current_key != -1){
                $('#input-'+id).val(valueArr[current_key]);            
            }
        });

        $('button.btn-plus').click(function(){
            id = $(this).data('id');
            current = $('#input-'+id).val();
            current_key = valueArr.indexOf(current);
            current_key++;
            if(current_key != 17){
                $('#input-'+id).val(valueArr[current_key]);            
            }
        });

        $('button.btn-edit').click(function(){
            url = $(this).data('url');
            $('#form-edit').attr('action',url);
        });

        function getValue(key,id){
            $('#input-'+id).val(valueArr[key]);
        }
    </script>
@endsection