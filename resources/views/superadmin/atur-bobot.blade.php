@extends('layouts.base-template')
@section('content')
    <div class="border rounded p-4 m-2">
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <th>No</th>
                    <th>Nama Kriteria</th>
                    <th>Aksi</th>
                </thead>
                <tbody>
                    @foreach($kriteria as $key => $bobot)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$bobot->nama_kriteria}}</td>
                        <td>
                            <a href="{{route('bobot.edit',[$bobot->id])}}" class="btn btn-primary btn-edit"><i class="fa fa-pencil"></i> Edit Bobot</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
