@extends('layouts.base-template')
@section('breadcrumb')
    <nav class="breadcrumb">
        <span class="breadcrumb-item active">Dashboard</span>
    </nav>
@endsection

@section('alerts')
    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        {{Session::get('success')}}
    </div>
    @endif
@endsection

@section('content')
    <div class="border rounded ">
        <div class="p-4">
            <h1>Buat Kriteria</h1>
            <form method="POST" action="">
                <div class="form-group">
                    <label for="kriteria"></label>
                    <input type="text" id="kriteria" name="nama_kriteria" value="{{old('nama_kriteria')}} " class="form-control {{$errors->has('nama_kriteria') ? 'is-invalid' : ''}}" />
                </div>
                @if ($errors->has('nama_kriteria'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('nama_kriteria') }}</strong>
                    </span>
                @endif
                @csrf
                <button class="btn btn-primary float-right" type="submit"><i class="fa fa-plus"></i> Tambah</button>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>
@endsection