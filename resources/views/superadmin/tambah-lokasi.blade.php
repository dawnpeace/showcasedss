@extends('layouts.base-template')

@section('alerts')
@if(Session::has('errors'))
<div class="alert alert-danger" role="alert">
    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
    @foreach (Session::get('errors')->all() as $error)
        <p>{{ $error }}</p>
    @endforeach
</div>
@endif

@if(Session::has('success'))
<div class="alert alert-success" role="alert">
    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
    {{Session::get('success')}}
</div>
@endif
@endsection

@section('content')
    <div class="border rounded p-4 m-2">
        <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#modal-add"><i class="fa fa-plus"></i></button>
        <h1>Daftar Lokasi</h1>
        <div class="row">
            @foreach($lokasi as $tempat)
            <div class="col-md-6 p-2">

                <div class="card mb-2">
                    <div class="card-body">
                        <h3>{{$tempat->nama_lokasi}}</h3>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-md-4">
                                    Konsep Acara
                                </div>
                                <div class="col-md-8">
                                    : {{$tempat->konsep_acara}}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-md-4">
                                    Sistem Sewa
                                </div>
                                <div class="col-md-8">
                                    : {{$tempat->sistem_sewa}}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-md-4">
                                    Lokasi
                                </div>
                                <div class="col-md-8">
                                    : {{$tempat->lokasi}}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-md-4">
                                    Media Publikasi
                                </div>
                                <div class="col-md-8">
                                    : {{$tempat->media_publikasi}}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-md-4">
                                    Jumlah Penyewa
                                </div>
                                <div class="col-md-8">
                                    : {{$tempat->jumlah_penyewa}}
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            @endforeach
        </div>
        
    </div>
@endsection

@section('modals')

    <!-- Modal -->
    <div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Lokasi</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <form action="" method="post">
                <div class="modal-body">
                        @csrf
                        <div class="form-group">
                            <label for="lokasi">Nama Lokasi</label>
                            <input type="text" id="lokasi" name="nama_lokasi" value="{{old('nama_lokasi')}}" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label for="loc">Alamat / Lokasi</label>
                            <input type="text" id="loc" name="lokasi" value="{{old('lokasi')}}" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label for="sistem_sewa">Sistem sewa</label>
                            <input type="text" id="sistem_sewa" name="sistem_sewa" value="{{old('sistem_sewa')}}" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label for="konsep_acara">Konsep Acara</label>
                            <input type="text" id="konsep_acara" name="konsep_acara" value="{{old('konsep_acara')}}" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label for="media_publikasi">Media Publikasi</label>
                            <input type="text" id="media_publikasi" name="media_publikasi" value="{{old('media_publikasi')}}" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label for="jumlah_penyewa">Jumlah Penyewa</label>
                            <input type="text" id="jumlah_penyewa" name="jumlah_penyewa" value="{{old('jumlah_penyewa')}}" class="form-control" required/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection