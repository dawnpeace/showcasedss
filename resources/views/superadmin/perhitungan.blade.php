@extends('layouts.base-template')

@section('alerts')
@if(Session::has('errors'))
<div class="alert alert-danger" role="alert">
    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
    @foreach (Session::get('errors')->all() as $error)
        <p>{{ $error }}</p>
    @endforeach
</div>
@endif

@endsection

@section('content')
    <div class="border rounded p-4 m-2">
        <h1>Perhitungan</h1>
        <hr>
        @if(!isset($lokasi))
        <form action="">
            <select class="form-control select2" name="lokasi[]" multiple="multiple">
                @foreach($daftarLokasi as $tempat)
                    <option value="{{$tempat->id}}">{{$tempat->nama_lokasi}}</option>
                @endforeach
            </select>
            <button class="btn btn-primary mt-2" type="submit">Submit</button>
        </form>
        @endif

        @isset($lokasi)
        <form action="{{route('hitung')}}" method="POST">
        <div class="row">
            @foreach($lokasi as $tempat)
                @foreach($lokasiInvers as $tempatInvers)
                    @if($tempatInvers->id != $tempat->id)
                    <div class="col-md-4 mb-3">
                        <h5>{{$tempat->nama_lokasi}} terhadap {{$tempatInvers->nama_lokasi}}</h5>
                        @foreach($kriteria as $kategori)
                            <div class="form-group">
                                <label for="">{{$kategori->nama_kriteria}}</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <button type="button" data-terhadap="{{$tempat->id.'-'.$tempatInvers->id.'-'.$kategori->id}}" type="button" class="btn btn-outline-info btn-minus"><i class="fa fa-minus"></i></button>
                                    </div>
                                    <input id="{{$tempat->id.'-'.$tempatInvers->id.'-'.$kategori->id}}" type="text" id="" name="bobot[{{$kategori->id}}][{{$tempat->id}}][{{$tempatInvers->id}}]" value="1" class="form-control" readonly/>
                                    <div class="input-group-append">
                                        <button type="button" data-terhadap="{{$tempat->id.'-'.$tempatInvers->id.'-'.$kategori->id}}" class="btn btn-outline-info btn-plus"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    @endif
                @endforeach
            @endforeach
        </div>
        @csrf
        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        @endisset
    </div>
@endsection

@section('js')
    <script>
        console.log(nilaiArr);
        $('.select2').select2({
            maximumSelectionLength: 4,
        });
        var nilaiArr = ["1/9","1/8","1/7","1/6","1/5","1/4","1/3","1/2","1","2","3","4","5","6","7","8","9"];
        var nilaiArrReverse = ["9", "8", "7", "6", "5", "4", "3", "2", "1", "1/2","1/3","1/4","1/5","1/6","1/7","1/8","1/9"]
        $('button.btn-plus').click(function(){
            id = $(this).data('terhadap');
            reverseId = id.split('-');
            reverseId = reverseId[1]+"-"+reverseId[0]+"-"+reverseId[2];
            currentPosition = nilaiArr.indexOf($("#"+id).val());
            plusInput = $('input#'+id);
            minusInput = $('input#'+reverseId);
            if(currentPosition < nilaiArr.length-1){
                plusInput.val(nilaiArr[currentPosition+1]);
                minusInput.val(nilaiArrReverse[currentPosition+1]);
            }
        });

        $('button.btn-minus').click(function(){
            id = $(this).data('terhadap');
            reverseId = id.split('-');
            reverseId = reverseId[1]+"-"+reverseId[0]+"-"+reverseId[2];
            currentPosition = nilaiArr.indexOf($("#"+id).val());
            plusInput = $('input#'+id);
            minusInput = $('input#'+reverseId);
            if(currentPosition > 0 ){
                plusInput.val(nilaiArr[currentPosition-1]);
                minusInput.val(nilaiArrReverse[currentPosition-1]);
            }
        });

    </script>
@endsection