@extends('layouts.base-template')
@section('content')
    <div class="border-rounded p-4 m-2">
        <h2>Eigen Kriteria</h2>
        <table class="table table-bordered mb-3">
            <tbody>
                @foreach($eigenKriteria as $key => $value)
                    <tr>
                        <th>{{$kriteria->get($key)['nama_kriteria']}}</th>
                        <td>{{$value}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <h2>Eigen Lokasi</h2>
        <table class="table table-sm table-bordered mb-3">
            <tbody>
                <tbody>
                        <tr>
                            <th>Kriteria</th>
                            @foreach(reset($eigenLokasi) as $key => $value)
                            <th>{{$lokasi->get($key)['nama_lokasi']}}</th>
                            @endforeach
                        </tr>
                    @foreach($eigenLokasi as $key => $value)
                        <tr>
                            <th>{{$kriteria->get($key)['nama_kriteria']}}</th>
                            @foreach($value as $lokasiKey => $lokasiBobot)
                            <td>{{$lokasiBobot}}</td>
                            @endforeach
                        </tr>
                    @endforeach
                </tbody>
            </tbody>
        </table>

        <h2>Hasil Kali Matrix</h2>
        <table class="table table-sm table-bordered">
            <tbody>
                @foreach($rangking["data"] as $key => $rank)
                <tr>
                    <th>{{$lokasi->get($key)['nama_lokasi'] . ($key == $rangking["rekomendasi"] ? " (Rekomendasi)" : "")}}</th>
                    <td>{{$rank}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection