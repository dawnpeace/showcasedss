<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SPK Pameran</title>
  <link href="{{asset('css/app.css')}}" rel="stylesheet">
  <script src="{{asset('js/app.js')}}"></script>
</head>

<body>

  <div class="d-flex" id="wrapper">
  @include('layouts.sidebar')
    <!-- Page Content -->
    <div id="page-content-wrapper">
    @include('layouts.navbar')
      <div class="container">
        <div class="my-2">
          @yield('breadcrumb')
        </div>
        <div class="my-2 mx-4">
          @yield('alerts')
        </div>
      </div>
      <div class="container-fluid">
        <div class="m-4">
          @yield('content')
        </div>
      </div>
    </div>
    <!-- /#page-content-wrapper -->

    @yield('modals')

  </div>
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>
  @yield('js')
</body>

</html>
