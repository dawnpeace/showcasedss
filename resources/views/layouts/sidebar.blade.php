<!-- Sidebar -->
<div class="bg-light border-right" id="sidebar-wrapper">
    <a href="{{url('')}}" class="link-unstyled"><div class="sidebar-heading">SPK Pameran</div></a>
    <div class="list-group list-group-flush">
        <a href="{{route('bobot.index')}}" class="list-group-item list-group-item-action bg-light"> <i class="fa fa-list"></i> Daftar Kategori</a>
        <a href="{{route('lokasi.create')}}" class="list-group-item list-group-item-action bg-light"> <i class="fa fa-map-o"></i> Daftar Data Lokasi</a>
        <a href="{{route('perhitungan.index')}}" class="list-group-item list-group-item-action bg-light"><i class="fa fa-calculator"></i> Perhitungan</a>
    </div>
</div>
<!-- /#sidebar-wrapper -->