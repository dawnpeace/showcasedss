<?php

namespace App;
use App\Kriteria;
use App\Bobot;


class AHP{
    // Normalized Array Matrix
    private $array;
    // Array Kriteria
    private $arrKriteria;
    // Array Kriteria yang telah dinormalisasi
    private $arrKriteriaNormal;
    // Jumlah kriteria
    private $jumlahKriteria;
    // Array of Matrix Sum
    private $arraySum;
    // Eigen Array
    private $eigenVector;
    // Bobot Evaluasi 
    private $bobotEvaluasiKriteria;
    // Bobot lokasi
    private $eigenLokasi;

    
    public function __construct(array $array)
    {
        $this->array = $this->normalizeArrayValue($array);

        $kriteria = Kriteria::with(['bobot'=>function($query){$query->with(['terhadap']);}])->get();
        
        $arrKriteria = [];
        foreach($kriteria as $kategori){
            foreach($kategori->bobot as $bobot){
                $arrKriteria[$kategori->id][$bobot->id_kriteria_terhadap] = $bobot->bobot;
            }
        }
        $this->arrKriteria =  $arrKriteria;
        $this->jumlahKriteria = Kriteria::count();
        $this->arrKriteria = $this->hitungEigen();
    }

    public function getArray()
    {
        return $this->array;
    }

    private function hitungEigen()
    {
        $tempArr = [];
        foreach($this->arrKriteria as $key => $kriteria){
            $tempArr[$key] = 0;
            foreach($this->arrKriteria as $innerKey => $innerKriteria){
                $tempArr[$key] += $this->arrKriteria[$innerKey][$key];
            }
        }
        $this->bobotEvaluasiKriteria = $tempArr;

        $kriteriaTernormalisasi = [];
        foreach($this->arrKriteria as $key => $terhadap){
            foreach($terhadap as $innerKey => $kriteria){
                $kriteriaTernormalisasi[$key][$innerKey] = $kriteria/$tempArr[$innerKey];
            }
        }

        // return $kriteriaTernormalisasi;
        $bobotKriteriaTernormalisasi = [];
        foreach($kriteriaTernormalisasi as $key => $kriteria){
            $bobotKriteriaTernormalisasi[$key] = array_sum($kriteria) / count($kriteria);
        }
        $this->eigenVector = $bobotKriteriaTernormalisasi;
    }

    public function sumBobotLokasi()
    {
        $arr = [];
        foreach($this->array as $key => $value){
            foreach($value as $innerKey => $innerValue){
                $arr[$key][$innerKey] = array_sum($innerValue);
            }
        }
        $this->arraySum = $arr;
        return $this;
    }

    public function getSumBobotLokasi()
    {
        return $this->arraySum ?? [];
    }


    public function getEigenVector()
    {
        return $this->eigenVector;
    }

    public function getEigenLokasi()
    {
        return $this->eigenLokasi;
    }

    /**
     * @param array bobotKriteria
     * bobot kriteria terhadap masing-masing lokasi
     */
    public function hitungBobotEigenLokasi(array $bobotKriteria)
    {
        $kriteria_kriteria = Kriteria::with('bobot')->get();
        //normalisasi array request
        foreach($bobotKriteria['bobot'] as $key => $kriteria){
            foreach($kriteria as $innerKey => $bobot){
                $bobotKriteria['bobot'][$key][$innerKey][$innerKey] = 1; 
            }
        }
        $bobotKriteria =  $this->normalizeArrayValue($bobotKriteria['bobot']);
        $sumBaris = [];
        // return $bobotKriteria;
        $temp =[];
        foreach($bobotKriteria as $key => $kriteria){
            foreach($kriteria as $innerKey => $lokasi){
                $temp[$innerKey] = [];
                foreach($kriteria as $secondKey => $secondLokasi){
                    $temp[$innerKey][] = $secondLokasi[$innerKey];
                }
            }
            $sumBaris[$key] = $temp;
        }
        $bobotLokasi = [];
        foreach($sumBaris as $key => $value){
            foreach($value as $innerKey => $innerValue){
                $bobotLokasi[$key][$innerKey] = array_sum($innerValue);
            }
        }

        // return $this->bobotLokasi = $bobotLokasi;

        $eigenMatrix = [];
        $eigenLokasi = [];
        foreach($bobotKriteria as $kriteriaKey => $valueLokasi){
            foreach($valueLokasi as $lokasiKey => $valueTerhadap){
                foreach($valueTerhadap as $terhadapKey => $terhadap){
                    $eigenMatrix[$kriteriaKey][$lokasiKey][$terhadapKey] = $terhadap/$bobotLokasi[$kriteriaKey][$terhadapKey];
                }
            }
        }
        
        foreach($eigenMatrix as $keyKriteria => $kriteria){
            foreach($kriteria as $keyLokasi => $arrLokasi){
                $eigenLokasi[$keyKriteria][$keyLokasi] = array_sum($arrLokasi) / count($arrLokasi);
            }
        }
        $this->eigenLokasi = $eigenLokasi;
        return $eigenLokasi;
    }

    public function getHasilRanking()
    {
        $result = [];
        foreach($this->eigenLokasi as $keyKriteria => $perKriteria)
        {
            foreach($perKriteria as $keyLokasi => $bobotLokasi)
            {
                if(!isset($result["data"][$keyLokasi])){
                    $result["data"][$keyLokasi] = $this->eigenVector[$keyKriteria] * $bobotLokasi;
                } else {
                    $result["data"][$keyLokasi] += $this->eigenVector[$keyKriteria] * $bobotLokasi;
                }
            }
        }

        $max = max($result["data"]);
        $result["rekomendasi"] = array_search($max,$result["data"]);
        return $result;
    }

    /**
     * Normalisasi konten array
     * 1/3 => 0,3333
     */
    private function normalizeArrayValue(array $array)
    {
        $normal = [
            "1/9" => 0.1111,
            "1/8" => 0.125,
            "1/7" => 0.1429,
            "1/6" => 0.1667,
            "1/5" => 0.2,
            "1/4" => 0.25,
            "1/3" => 0.333333333,
            "1/2" => 0.5,
            "1" => 1,
            "2" => 2,
            "3" => 3,
            "4" => 4,
            "5" => 5,
            "6" => 6,
            "7" => 7,
            "8" => 8,
            "9" => 9 
        ];

        $normalizedArray = [];

        foreach($array as $key => $value){
            foreach($value as $innerKey => $innerValue){
                foreach($value as $secondInnerKey => $secondInnerValue){
                    $unnormalized = $array[$key][$innerKey][$secondInnerKey];
                    $normalizedArray[$key][$innerKey][$secondInnerKey] = $normal[$unnormalized];
                }
            }
        }
        return $normalizedArray;
    }

    
}