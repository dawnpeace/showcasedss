<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bobot extends Model
{
    protected $table = "tb_nilai_bobot";
    protected $fillable = ['id_kriteria','id_kriteria_terhadap','bobot'];

    public function kriteria()
    {
        return $this->belongsTo('App\Kriteria','id_kriteria');
    }

    public function terhadap()
    {
        return $this->belongsTo('App\Kriteria','id_kriteria_terhadap');
    }


}
