<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lokasi extends Model
{
    protected $table = 'tb_lokasi';
    protected $fillable = ['nama_lokasi','lokasi','sistem_sewa','media_publikasi','jumlah_penyewa','konsep_acara'];
}
