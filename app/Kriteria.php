<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kriteria extends Model
{
    protected $table = "tb_kriteria";
    protected $fillable = ['nama_kriteria'];

    public function bobot()
    {
        return $this->hasMany('App\Bobot','id_kriteria');
    }

    public function terhadap()
    {
        return $this->hasMany('App\Bobot','id_kriteria_terhadap');
    }
}
