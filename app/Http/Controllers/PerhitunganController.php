<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kriteria;
use App\Lokasi;
use App\AHP;

class PerhitunganController extends Controller
{
    public function index(Request $request)
    {
        $request->validate(['lokasi'=>'nullable|array|min:2|max:4']);
        if($request->has('lokasi')){
            $lokasi = Lokasi::whereIn('id',$request->lokasi)->get();
            return $this->tampilDaftarLokasi($lokasi);
        }
        $lokasi = Lokasi::all();
        return view('superadmin.perhitungan',['daftarLokasi'=>$lokasi]);
    }

    public function lokasi($kriteria)
    {
        return $kriteria;
    }

    public function hitung(Request $request)
    {
        $bobot = $request->bobot;
        $arrNilai = [];
        foreach($bobot as $key => $nilai){
            foreach($nilai as $idLokasi => $lokasi){
                foreach($nilai as $idLokasiReverse => $lokasiReverse){
                    $arrNilai[$key][$idLokasi][$idLokasiReverse] = $lokasi[$idLokasiReverse] ?? 1;
                }
            }
        }
        $ahp = new AHP($arrNilai);
        // return $ahp->getEigenVector();
        $kriteria = Kriteria::all()->keyBy('id');
        $lokasi = Lokasi::all()->keyBy('id');
        $eigenLokasi = $ahp->hitungBobotEigenLokasi($request->all());
        $eigenKriteria = $ahp->getEigenVector();
        $rangking = $ahp->getHasilRanking();

        return view('superadmin.hasil-perhitungan',compact(["kriteria","eigenLokasi","eigenKriteria","lokasi","rangking"]));
    }

    private function tampilDaftarLokasi($lokasi)
    {
        $kriteria = Kriteria::with(['bobot'=>function($query){
            $query->with(['terhadap']);
        }])->get();
        $lokasiInvers = $lokasi->reverse();
        return view('superadmin.perhitungan',['kriteria'=>$kriteria,'lokasi'=>$lokasi,'lokasiInvers'=>$lokasiInvers]);
    }

    
}
