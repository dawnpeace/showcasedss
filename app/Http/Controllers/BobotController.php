<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kriteria;
use App\Bobot;
use Illuminate\Validation\Rule;

class BobotController extends Controller
{
    public function index()
    {
        $kriteria = Kriteria::with(['bobot'])->get();
        return view('superadmin.atur-bobot',compact('kriteria'));
    }

    public function edit(Kriteria $kriteria)
    {
        $kriteria->with(['bobot'=>function($query){
            $query->with(['kriteria']);
        }])->get();
        $bobot = ['1/9' => 0.1111111,
            '1/8' => 0.125,
            '1/7' => 0.1429,
            '1/6' => 0.1667,
            '1/5' => 0.2,
            '1/4' => 0.25,
            '1/3' => 0.3333333333,
            '1/2' => 0.5,
            '1' => 1,
            '2' => 2,
            '3' => 3,
            '4' => 4,
            '5' => 5,
            '6' => 6,
            '7' => 7,
            '8' => 8,
            '9' => 9
        ];
        return view('superadmin.bobot-terhadap',['bobot'=>$bobot , 'terhadap' => $kriteria]);
    }

    public function update(Kriteria $kriteria1, Kriteria $kriteria2, Request $request)
    {
        $bobot = ['1/9' => 0.1111,
            '1/8' => 0.125,
            '1/7' => 0.1429,
            '1/6' => 0.1667,
            '1/5' => 0.2,
            '1/4' => 0.25,
            '1/3' => 0.3333,
            '1/2' => 0.5,
            '1' => 1,
            '2' => 2,
            '3' => 3,
            '4' => 4,
            '5' => 5,
            '6' => 6,
            '7' => 7,
            '8' => 8,
            '9' => 9
        ];
        $request->validate(['bobot'=>
            [
                'required',
                Rule::in(array_keys($bobot))
            ]
        ]);

        if($kriteria1->id == $kriteria2->id && $request->bobot != 1){
            return redirect()->back()->with("errors",collect(["bobot"=>"Same Category can only have 1 as value"]));
        }
        
        $nilaiTerhadap = explode('/',$request->bobot);
        $bobotTerhadap = count($nilaiTerhadap) == 2 ? $nilaiTerhadap[1] : "1/$nilaiTerhadap[0]";
        
        Bobot::updateOrCreate(
            ['id_kriteria'=>$kriteria1->id,'id_kriteria_terhadap'=>$kriteria2->id],
            ['bobot'=>$bobot[$request->bobot]]
        );

        Bobot::updateOrCreate(
            ['id_kriteria'=>$kriteria2->id,'id_kriteria_terhadap'=>$kriteria1->id],
            ['bobot'=>$bobot[$bobotTerhadap]]
        );
            
        return redirect()->back()->with('success','Bobot berhasil diubah');
    }
}
