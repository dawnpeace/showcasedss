<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lokasi;

class LokasiController extends Controller
{
    public function create()
    {
        $lokasi = Lokasi::all();
        return view('superadmin.tambah-lokasi',compact('lokasi'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'lokasi' => 'required|string',
            'nama_lokasi' => 'required|string',
            'konsep_acara' => 'required|string',
            'sistem_sewa' => 'required|string',
            'media_publikasi' => 'required|string',
            'jumlah_penyewa' => 'required|string',
        ]);
        Lokasi::create($request->all());
        return redirect()->back()->with('success','Lokasi berhasil ditambahkan');
    }   
}
