<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kriteria;

class KriteriaController extends Controller
{
    public function index()
    {
        return view();
    }

    public function create()
    {
        return view('superadmin.buat-kriteria');
    }

    public function store(Request $request)
    {
        $request->validate(["nama_kriteria"=>"required|string|max:191"]);
        Kriteria::create(["nama_kriteria"=>$request->nama_kriteria]);
        return redirect()->back()->with('success','Kriteria berhasil ditambahkan');
    }
}
