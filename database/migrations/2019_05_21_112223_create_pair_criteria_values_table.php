<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePairCriteriaValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_nilai_bobot', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_kriteria');
            $table->foreign('id_kriteria')->references('id')->on('tb_kriteria')->onDelete('cascade');
            $table->unsignedBigInteger('id_kriteria_terhadap');
            $table->foreign('id_kriteria_terhadap')->references('id')->on('tb_kriteria')->onDelete('cascade');
            $table->tinyInteger('bobot');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_nilai_bobot');
    }
}
