<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            "email" => "admin@mail.com",
            "password" => bcrypt('rahasia'),
            "name" => "Khairini",
        ]);
    }
}
