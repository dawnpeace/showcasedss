<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::redirect('/','admin/atur-bobot');
Route::prefix('admin')->middleware('auth')->group(function(){
    Route::get('/atur-bobot','BobotController@index')->name('bobot.index');

    Route::get('/kriteria/{kriteria}','BobotController@edit')->name('bobot.edit');
    Route::post('/kriteria/{kriteria1}/terhadap/{kriteria2}','BobotController@update')->name('bobot.update');

    Route::get('/lokasi','LokasiController@create')->name('lokasi.create');
    Route::post('/lokasi','LokasiController@store')->name('lokasi.store');
    
    Route::get('/perhitungan','PerhitunganController@index')->name('perhitungan.index');
    Route::post('/hasil','PerhitunganController@hitung')->name('hitung');
});

Route::namespace('Auth')->group(function(){
    Route::get('login','LoginController@showLoginForm');
    Route::post('login','LoginController@login')->name('login');
    Route::post('logout','LoginController@logout')->name('logout');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/home', 'HomeController@index')->name('home');
